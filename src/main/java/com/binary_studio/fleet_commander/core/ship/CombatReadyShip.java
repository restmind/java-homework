package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.NamedEntity;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private final Integer MAX_VALUE_OF_CAPACITOR;

	private final Integer MAX_VALUE_OF_SHIELDHP;

	private final Integer MAX_VALUE_OF_HULLHP;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
		this.MAX_VALUE_OF_CAPACITOR = capacitorAmount.value();
		this.MAX_VALUE_OF_SHIELDHP = shieldHP.value();
		this.MAX_VALUE_OF_HULLHP = hullHP.value();
	}

	@Override
	public void endTurn() {
		if (this.capacitorAmount.value() + this.capacitorRechargeRate.value() >= this.MAX_VALUE_OF_CAPACITOR) {
			this.capacitorAmount = PositiveInteger.of(this.MAX_VALUE_OF_CAPACITOR);
		}
		else {
			this.capacitorAmount = PositiveInteger
					.of(this.capacitorAmount.value() + this.capacitorRechargeRate.value());
		}
	}

	@Override
	public void startTurn() {
		// TODO: Ваш код здесь :)

	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.capacitorAmount.value() - this.attackSubsystem.getCapacitorConsumption().value() >= 0) {
			this.capacitorAmount = PositiveInteger
					.of(this.capacitorAmount.value() - this.attackSubsystem.getCapacitorConsumption().value());
			return Optional.of(new AttackAction(this.attackSubsystem.attack(target), (NamedEntity) this,
					(NamedEntity) target, (NamedEntity) this.attackSubsystem));
		}
		return Optional.empty();
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction result = this.defenciveSubsystem.reduceDamage(attack);
		if (this.shieldHP.value() - result.damage.value() >= 0) {
			this.shieldHP = PositiveInteger.of(this.shieldHP.value() - result.damage.value());
		}
		else if (this.hullHP.value() + this.shieldHP.value() - result.damage.value() >= 0) {
			this.hullHP = PositiveInteger.of(this.hullHP.value() + this.shieldHP.value() - result.damage.value());
			this.shieldHP = PositiveInteger.of(0);
		}
		else {
			return new AttackResult.Destroyed();
		}
		return new AttackResult.DamageRecived(result.weapon, result.damage, result.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.capacitorAmount.value() - this.defenciveSubsystem.getCapacitorConsumption().value() >= 0) {
			this.capacitorAmount = PositiveInteger
					.of(this.capacitorAmount.value() - this.defenciveSubsystem.getCapacitorConsumption().value());
			RegenerateAction regenerateAction = this.defenciveSubsystem.regenerate();

			PositiveInteger regeneratedShieldHP = new PositiveInteger(0);
			if (this.shieldHP.value() + regenerateAction.shieldHPRegenerated.value() > this.MAX_VALUE_OF_SHIELDHP) {
				regeneratedShieldHP = PositiveInteger.of(this.MAX_VALUE_OF_SHIELDHP - this.shieldHP.value());
				this.shieldHP = PositiveInteger.of(this.MAX_VALUE_OF_SHIELDHP);
			}
			else if (this.shieldHP.value()
					+ regenerateAction.shieldHPRegenerated.value() < this.MAX_VALUE_OF_SHIELDHP) {
				regeneratedShieldHP = PositiveInteger.of(regenerateAction.shieldHPRegenerated.value());
				this.shieldHP = PositiveInteger
						.of(this.shieldHP.value() + regenerateAction.shieldHPRegenerated.value());
			}

			PositiveInteger regeneratedHullHP = new PositiveInteger(0);
			if (this.hullHP.value() + regenerateAction.hullHPRegenerated.value() > this.MAX_VALUE_OF_HULLHP) {
				regeneratedHullHP = PositiveInteger.of(this.MAX_VALUE_OF_HULLHP - this.hullHP.value());
				this.hullHP = PositiveInteger.of(this.MAX_VALUE_OF_HULLHP);
			}
			else if (this.hullHP.value() + regenerateAction.hullHPRegenerated.value() < this.MAX_VALUE_OF_HULLHP) {
				regeneratedHullHP = PositiveInteger.of(regenerateAction.hullHPRegenerated.value());
				this.hullHP = PositiveInteger.of(this.hullHP.value() + regenerateAction.hullHPRegenerated.value());
			}
			return Optional.of(new RegenerateAction(regeneratedShieldHP, regeneratedHullHP));
		}
		return Optional.empty();
	}

}
