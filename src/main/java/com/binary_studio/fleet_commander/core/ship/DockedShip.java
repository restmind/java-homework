package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		int result = 0;
		if (subsystem != null) {
			result = this.powergridOutput.value() - subsystem.getPowerGridConsumption().value();
			if (result >= 0) {
				this.attackSubsystem = subsystem;
				this.powergridOutput = PositiveInteger.of(result);
			}
			else {
				throw new InsufficientPowergridException(result);
			}
		}
		else {
			this.attackSubsystem = null;
		}

	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		int result = 0;
		if (subsystem != null) {
			result = this.powergridOutput.value() - subsystem.getPowerGridConsumption().value();
			if (result >= 0) {
				this.defenciveSubsystem = subsystem;
				this.powergridOutput = PositiveInteger.of(result);
			}
			else {
				throw new InsufficientPowergridException(result);
			}
		}
		else {
			this.defenciveSubsystem = null;
		}

	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.defenciveSubsystem != null && this.attackSubsystem != null) {
			return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.powergridOutput,
					this.capacitorAmount, this.capacitorRechargeRate, this.speed, this.size, this.attackSubsystem,
					this.defenciveSubsystem);
		}
		else if (this.defenciveSubsystem == null && this.attackSubsystem != null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		else if (this.defenciveSubsystem != null && this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else {
			throw NotAllSubsystemsFitted.bothMissing();
		}
	}

}
