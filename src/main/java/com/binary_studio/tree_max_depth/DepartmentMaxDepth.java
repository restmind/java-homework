package com.binary_studio.tree_max_depth;

import java.util.ArrayDeque;
import java.util.Queue;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}
		Queue<Department> queue = new ArrayDeque<>();
		queue.add(rootDepartment);

		Department front;
		int height = 0;

		while (!queue.isEmpty()) {

			int size = queue.size();

			while (size-- > 0) {
				front = queue.poll();

				for (Department department : front.subDepartments) {
					if (department != null) {
						queue.add(department);
					}
				}
			}
			height++;
		}
		return height;
	}

}
