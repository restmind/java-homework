package com.binary_studio.uniq_in_sorted_stream;

import java.util.Comparator;
import java.util.function.Predicate;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		Comparator<Row<T>> comparator = new Comparator<>() {
			@Override
			public int compare(Row<T> o1, Row<T> o2) {
				return 0;
			}
		};

		Row<T> number = new Row<>(Long.MIN_VALUE);
		Predicate<Row<T>> isBigger = new Predicate<>() {
			@Override
			public boolean test(Row<T> o) {
				if (o.getPrimaryId() > number.getPrimaryId()) {
					number.setPrimaryId(o.getPrimaryId());
					return true;
				}
				else {
					return false;
				}
			}
		};
		return stream.sorted(comparator).filter(isBigger);
	}

}
